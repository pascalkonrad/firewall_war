package at.fireballwar.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Spielfigur implements Actor {

	public float x = 700, y = 100;
	public float xs, ys = -50;
	private Image image;
	private Image imageS;
	private boolean keyescape, keydown, keyup, shoot, b;
	// private boolean isVisible;

	Spielfigur() throws SlickException {

		this.image = new Image("testdata/wizard1.png");
		this.imageS = new Image("testdata/geschoss1.png");
		// this.x = x;
		// this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		
		this.keyescape = gc.getInput().isKeyDown(Input.KEY_ESCAPE);
		this.keyup = gc.getInput().isKeyDown(Input.KEY_UP);
		this.shoot = gc.getInput().isKeyDown(Input.KEY_M);
		this.keydown = gc.getInput().isKeyDown(Input.KEY_DOWN);
		if(this.y < 1500) {
		if (this.keydown == true && y < 500) {
			this.y++;
		} else if (this.keyup == true && y > 0) {
			this.y--;
		} else if (this.keyescape == true) {
			System.exit(0);
		} else if (this.shoot == true) {
			this.b = true;
			this.xs = this.x;
			this.ys = this.y;
			System.out.println(this.xs);
		}
		}
		else if(this.y > 1500 ) {
			if (this.keydown == true ) {
				this.y = 1500;
			}
			
			
		}

		if (this.b == true && this.xs < 850) {
			shoot(delta);
			// System.out.println(this.xs);
			// System.out.println(this.ys);

		} else if (this.xs > 850) {
			this.b = false;

		}
		}
	

	@Override
	public void render(Graphics graphics, GameContainer gc) {
		// TODO Auto-generated method stub
		
		graphics.drawImage(image, x, y);
		
		graphics.drawImage(imageS, xs, ys);

	}

	private void shoot(int geschoss) {

		this.xs--;

	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getXs() {
		return xs;
	}

	public float getYs() {
		return ys;
	}
  
}
