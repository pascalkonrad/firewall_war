package at.fireballwar.main;


import java.util.ArrayList;
import java.util.List;


import javax.swing.JOptionPane;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.BigImage;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Maingane extends BasicGame {
	public boolean Visible = true;
	private Image Background;
	private List<Actor> actors;
	private Spielfigur figureRight;
	private Spielfigur2 figureLeft;
	

	public Maingane(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actors : this.actors) {
			actors.render(graphics, gc);
		}
		Background.draw(0, 0, new Color(1, 1, 1, 0.5f));
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.actors = new ArrayList<>();
		this.figureRight = new Spielfigur();
		this.actors.add(this.figureRight);

		this.figureLeft = new Spielfigur2();
		this.actors.add(this.figureLeft);
		// TODO Auto-generated method stub
		Background = new BigImage("testdata/background.png");

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub

		for (Actor actors : this.actors) {
			actors.update(gc, delta);

		}
		if (this.figureRight.ys > this.figureLeft.y - 20 && this.figureRight.ys <= this.figureLeft.y + 125
				&& this.figureRight.xs > this.figureLeft.x && this.figureRight.xs < this.figureLeft.x + 78) {

			// System.out.println("funktioniert");
			this.figureLeft.y= 1500;
			
			JOptionPane.showMessageDialog(

					null, "der blaue zauberkünstler hat gewonnen!!");

		}
		
	

		if (this.figureLeft.ye > this.figureRight.y - 20 && this.figureLeft.ye <= this.figureRight.y + 125
				&& this.figureLeft.xe > this.figureRight.x && this.figureLeft.xe < this.figureRight.x + 78) {
			this.figureRight.y = 1500;
			JOptionPane.showMessageDialog(

					null, "der lila zauberkünstler hat gewonnen!!");

		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Maingane("Spielfigur"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
	}
	
	

}
